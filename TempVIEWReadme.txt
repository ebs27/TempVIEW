TempVIEW 2022
Created and Maintained by: Eric Stach (ebs27@duke.edu)
Duke University, Mechanical Engineering & Materials Science
___________________________________________________________________________
Application Usage:
This application is used for temperature measurement and logging of up to
10 K-type thermocouples. It also has the ability to view previous datasets. 

CURRENTLY THE APPLICATION ONLY SUPPORTS NI 9213 and NI 9212 DAQ CARDS. 
The software autodetects which slot the module is in, and will default to
the 9213 if both are present. If you are using a simulated device, you
will need to select the slot number from the dropdown in the pop-up menu.
___________________________________________________________________________
Application Architecture:
This application was created with NI LabVIEW (2021) and DQMH. In consists of six DQMH Modules: 
	1. DAQ: handles the acquisition
	2. DataViewer: handles opening and viewing previous file sets
	3. Fun: does surprise fun things!
	4. GUI: Acts as the main user interface
	5. Logger: handles file management tasks
	6. Settings: updates DAQ and HW settings
___________________________________________________________________________
Installation Steps:
If you want to install the application only:
-At the top of the GitLab repository page, click Release.  
-Find the most recent Package (tempview-2022_version#.nikg)
-Download the .nipkg file. This is an NI Package, which installs through
the NI Package Manager.

If you want access to the source code:
-Download the Source Code via the releases page. Or, clone/fork the respository!
____________________________________________________________________________
Future Features:
These may or may not be in development, but this is mostly a list of
things I'd like to add in the future:
for spring 2023
-move the settings button to the where the settings are displayed...create more
intuative temp changing
-ability to change thermocouple headings in the display (ex. thermo 1 = abient) 
and/or add headers to datafile

-Configure different thermocouple types/external CJC
-Support other analog input modules
-Support non-NI instruments
-Auto-detect and install package updates
